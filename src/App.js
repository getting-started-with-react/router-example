import React, { Component } from 'react';
import Home from './pages/Home';
import About from './pages/About';
import {Route} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
        </header>
      </div>
    );
  }
}

export default App;
