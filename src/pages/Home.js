import React from 'react';

class Home extends React.Component {
    render() {
        return(
            <h1>Hello world from Home!</h1>
        )
    }
}

export default Home;